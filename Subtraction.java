package calculator;

public class Subtraction implements Numbers {
    @Override
    public double calculateValue(double var1,double var2)
    {
        return var1-var2;
    }
}
