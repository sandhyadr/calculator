package calculator;

import java.util.HashMap;
import java.util.Map;

public class Calculator {

    private Map<Character, Numbers> operationMap = new HashMap<Character, Numbers>();


   public  Calculator() {
        operationMap.put('+', new Addition());
        operationMap.put('-', new Subtraction());
        operationMap.put('*', new Multiplication());
        operationMap.put('/', new Division());
    }

    double makeCalculation(double var1, double var2, char operation)
    {
        Numbers operationMapValue = operationMap.get(operation);

        return operationMapValue.calculateValue(var1, var2);
    }
}

