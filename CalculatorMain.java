package calculator;

import java.util.Scanner;

public class CalculatorMain {

    public static void main(String[] args) {

        char operator = 0;
        double var1 = 0;
        double var2 = 0;


        Scanner scanner = new Scanner(System.in);
        System.out.println("Write two numbers ");

        try {
            var1 = scanner.nextDouble();
            var2 = scanner.nextDouble();

            System.out.println("Type the operation to perform: +, -, * , / sign");
            operator = scanner.next().charAt(0);
        } catch (Exception e) {
            System.out.println("invalid input");
        }

        //call the class with the method
        Calculator calculator = new Calculator();
        System.out.println(calculator.makeCalculation(var1, var2, operator));
    }
}
